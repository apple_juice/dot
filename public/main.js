window.addEventListener('DOMContentLoaded', function (){
  'use strict';

  var $body = $('body');

  // START OF: is mobile =====
  function isMobile() {
    return (/Android|iPhone|iPad|iPod|BlackBerry/i).test(navigator.userAgent || navigator.vendor || window.opera);
  }
  // ===== END OF: is mobile

  // START OF: mark is mobile =====
  (function() {
    if(isMobile()){
      $body.addClass('body--mobile');
    }else{
      $body.addClass('body--desktop');
    }
  })();
  // ===== END OF: mark is mobile


  $('._fileupload_input').on('change', function(){
    $('._fileupload_btn').text($('#file').val().replace(/^.+\\/,""));
  });

  function changingTitle() {
    var docTitle = $('.barba-container').data('title');
    document.title = docTitle;
  }

  changingTitle()

  var parallax = (function(){
    var parallaxInstance, isInit;

    function init(){
      var scene = document.getElementById('scene');
      parallaxInstance = new Parallax(scene);
      isInit = true;
    }
    function destroy(){
      if (isInit) {
        parallaxInstance.destroy()
        isInit = false;
      }
    }
    return {
      init: init,
      destroy: destroy
    }
  }());

  parallax.init();

  var pixi = (function(){
    function init(){
      if ($('.section_bg_canvas').length) {
        var spriteImages 	= document.querySelectorAll( '.section_image' );
        var spriteImagesSrc = [];

        for ( var i = 0; i < spriteImages.length; i++ ) {
          var img = spriteImages[i];
          spriteImagesSrc.push( img.getAttribute('src' ) );
        }

        var initCanvasSlideshow = new CanvasSlideshow({
          sprites: spriteImagesSrc,
          displacementImage: '/images/dmaps/2048x2048/clouds.jpg',
          autoPlay: true,
          fullScreen: true,
          autoPlaySpeed: [0, 6],
          displaceScale: [5000, 10000],
        });
      }
    }
    return {
      init: init,
    }
  }());

  pixi.init()

  // START OF: loader =====
  var loader = (function(){
    var $introImagesSection = $('.js-intro-image-section');
    var $loader = $('.js-loader-section');
    var durations = {
      revealIntro: 1700, //taken from CSS
      showIntroSlider: 1000,
      showIntroSlides: 1000,
      introSlidesShift: 1500,
      percentFakeIncrementing: 1000
    };
    var $percent = $('.js-percent');
    var $finishingOverlay = $('.js-loader-finishing-overlay');

    var currentPercentIteration = 0;
    var incrementor = 1.5;
    var newPercent;

    $body.addClass('state-fixed-body');

    function revealIntro() {
      $body.addClass('state-reveal-intro');

      //show slider children itself
      setTimeout(function(){
        $body.removeClass('state-fixed-body');
        $('.loader__pulse').remove();
        $body.addClass('animation-loaded animate-texts');
      }, durations.revealIntro);
    }

    function init(){

      $introImagesSection.imagesLoaded({ background: false })
      //reveal the into once all the images loaded
        .always( function( instance ) {
          setTimeout(function(){
            revealIntro();
          }, durations.percentFakeIncrementing);
        })
        .done( function( instance ) {
          console.log('All images successfully loaded.');
        })
        .fail( function() {
          console.log('Images loaded, at least one is broken.');
        })
        .progress( function( instance, image ) {
        });
    }
    function remove(){
      $body.removeClass('animation-loaded');
    }
    return {
      revealIntro: revealIntro,
      init: init,
      remove: remove
    }
  }());

  loader.init();
  // ===== END OF: loader

  var modal = (function(){
    function init(){

    }

    function show(){

    }

    function hide(){
      $body.removeClass('animation-loaded');
    }

    return {
      init: init,
      show: show,
      hide: hide
    }
  }());

  modal.init();

  // START OF: purchase features hover effect =====
  var openMenu = (function(){
    var $menuButton = $('.js-open-menu-button');

    function bind() {
      $(document).on('click', '.js-hide-feature', function(event) {
        if(!$(event.target).hasClass('js-show-feature')){
          $features.removeClass('state-opened');
        }
      });
      $menuButton.on('click', function(event) {
        event.preventDefault();
        event.stopPropagation();

        $(this).toggleClass('active');
        $body.toggleClass('menu-opened');
      });

    }

    function init(){
      bind();
    }
    return {
      init: init
    }
  }());

  openMenu.init();
  // ===== END OF: purchase features hover effect

  var currentNav = (function () {
    function bind() {
      var path = window.location.pathname.split("/").pop(), target = '';
      if ( path === '' || path === 'index.html' ) {
        path = 'index.html';
        $('.header_nav li').removeClass('header_nav__item--active');
      }

      target = $('.header_nav a[href="'+path+'"]');
      target.parents('li').addClass('header_nav__item--active').siblings().removeClass('header_nav__item--active');
    }

    function init(){
      bind();
    }

    return {
      init: init
    }
  }());

  currentNav.init();

  var changeLang = (function () {
    function bind() {
      $('._lang_cn').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        window.location.pathname = 'cn/' + window.location.pathname.split("/").pop()
      });

      $('._lang_ru').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        window.location.pathname = 'ru/' + window.location.pathname.split("/").pop()
      });

      $('._lang_eng').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        window.location.pathname = window.location.pathname.split("/").pop()
      });
    }

    function init(){
      bind();
    }

    return {
      init: init
    }
  }());

  changeLang.init();

  var pageScroll = (function () {
    function bind() {
      $('.section_text').bind('mousewheel', function(evt) {
        var delta = evt.originalEvent.wheelDelta,
          scrollTop = this.scrollTop
        if (scrollTop <= 0 && delta > 30) {
          $('.inner_page, .index_page').addClass('scrolling_up').removeClass('scrolling_down')
        }
      });

      $('.btn_close_text').on('click', function (e) {
        e.preventDefault();
        $('.inner_page, .index_page').addClass('scrolling_up').removeClass('scrolling_down')
      });

      $('._scroll').bind('mousewheel', function(evt) {
        var delta = evt.originalEvent.wheelDelta;

        if (delta > 0) {
          $('.inner_page').addClass('scrolling_up').removeClass('scrolling_down')
          $('.section_text').animate({ scrollTop: 0 }, 0);
        }
        else {
          $('.inner_page').addClass('scrolling_down').removeClass('scrolling_up')
        }
      });

      $('.scroll_to_explore').on('click', function (e) {
        e.preventDefault();
        $('.inner_page').addClass('scrolling_down').removeClass('scrolling_up')
      });
    }

    function init(){
      bind();
    }

    return {
      init: init
    }
  }());

  pageScroll.init()

  var general = (function(){
    var lastElementClicked;
    var PrevLink = document.querySelector('a.barba_prev');
    var NextLink = document.querySelector('a.barba_next');

    function bind() {
      Barba.Pjax.init();
      Barba.Prefetch.init();

      Barba.Dispatcher.on('linkClicked', function(el) {
        lastElementClicked = el;
      });

      Barba.Dispatcher.on('newPageLoaded', function(HTMLElementContainer) {
        changingTitle()
      });

      var MovePage = Barba.BaseTransition.extend({
        start: function() {
          this.originalThumb = lastElementClicked;

          Promise
            .all([this.newContainerLoading, this.scrollTop()])
            .then(this.movePages.bind(this));
        },

        scrollTop: function() {
          var deferred = Barba.Utils.deferred();
          var obj = { y: window.pageYOffset };

          TweenLite.to(obj, 0.4, {
            y: 0,
            onUpdate: function() {
              if (obj.y === 0) {
                deferred.resolve();
              }

              window.scroll(0, obj.y);
            },
            onComplete: function() {
              deferred.resolve();
            }
          });

          return deferred.promise;
        },

        movePages: function() {
          var _this = this;
          var goingForward = true;
          // this.updateLinks();

          var newPageFile = this.getNewPageFile()
          if (newPageFile === '' || newPageFile === this.oldContainer.dataset.prev) {
            goingForward = false;
          }

          $body.removeClass('menu-opened scrolling_up scrolling_down');
          $('.js-open-menu-button').removeClass('active');
          $('.section').removeClass('toggle_images');
          currentNav.init();
          pageScroll.init();

          TweenLite.set(this.newContainer, {
            visibility: 'visible',
            xPercent: goingForward ? 100 : -100,
            position: 'fixed',
            left: 0,
            top: 0,
            right: 0
          });

          TweenLite.to(this.oldContainer, 1.2, { xPercent: goingForward ? -100 : 100 });
          TweenLite.to(this.newContainer, 1.2, { xPercent: 0, onComplete: function() {
              TweenLite.set(_this.newContainer, { clearProps: 'all' });
              _this.done();
            }});
        },

        /*        updateLinks: function() {
                  PrevLink.href = this.newContainer.dataset.prev;
                  NextLink.href = this.newContainer.dataset.next;
                },*/

        getNewPageFile: function() {
          return Barba.HistoryManager.currentStatus().url.split('/').pop();
        }
      });

      Barba.Pjax.getTransition = function() {
        return MovePage;
      };
    }

    function init(){
      bind();
    }
    return {
      init: init
    }
  }());

  general.init();

  var lead = (function () {
    function bind() {
      $('.open_about_text').on('click', function (e) {
        e.preventDefault();
        $('.index_page').addClass('scrolling_down').removeClass('scrolling_up');
      });
    }

    function init(){
      bind();
    }
    return {
      init: init
    }
  }());

  lead.init();


  var inner = (function(){
    function bind() {
      if ($('.section').find('video').length) {
        $('.section').find('video').get(0).pause();
      }

      $('.btn_rotate').on('click', function (e) {
        e.preventDefault();

        if ($(this).parents('.section').hasClass('toggle_images')) {
          $(this).parents('.section').removeClass('toggle_images');
          if ($('.section').find('video').length) {
            $('.section').find('video').get(0).pause();
          }
        }
        else {
          $(this).parents('.section').addClass('toggle_images');
          if ($('.section').find('video').length) {
            $('.section').find('video').get(0).play();
          }
        }
      });

      $('.btn_rotate').on('mouseover', function () {
        $(this).removeClass('show_hint');
      });

      setInterval(function(){
        $('.btn_rotate').toggleClass("show_hint");
      }, 6000);

    }

    function init(){
      bind();
    }
    return {
      init: init
    }
  }());

  inner.init();

  var Home = Barba.BaseView.extend({
    namespace: 'home',
    onEnter: function() {
      $body.removeClass('inner_page');
      $body.addClass('index_page');

      lead.init();
    },
    onEnterCompleted: function() {
      $body.addClass('animate-texts');
      parallax.init();
    },
    onLeave: function() {
      parallax.destroy();
      $body.removeClass('index_page animate-texts');
    },
    onLeaveCompleted: function() {}
  });


  var Logistics = Barba.BaseView.extend({
    namespace: 'logistics',
    onEnter: function() {
      $body.addClass('inner_page');
      $body.removeClass('index_page');

      inner.init();
      pixi.init()
    },
    onEnterCompleted: function() {
      parallax.init();
      $body.addClass('animate-texts');
    },
    onLeave: function() {
      $body.removeClass('inner_page animate-texts');
      parallax.destroy();
    },
    onLeaveCompleted: function() {
    }
  });

  var Spirit = Barba.BaseView.extend({
    namespace: 'spirit',
    onEnter: function() {
      $body.addClass('inner_page');
      $body.removeClass('index_page');

      inner.init();
      pixi.init()
    },
    onEnterCompleted: function() {
      parallax.init();
      $body.addClass('animate-texts');
    },
    onLeave: function() {
      $body.removeClass('inner_page animate-texts');
      parallax.destroy();
    },
    onLeaveCompleted: function() {
    }
  });

  var Inner = Barba.BaseView.extend({
    namespace: 'inner',
    onEnter: function() {
      $body.addClass('inner_page');
      $body.removeClass('index_page');
      inner.init();
    },
    onEnterCompleted: function() {
      parallax.init();
      $body.addClass('animate-texts');
    },
    onLeave: function() {
      $body.removeClass('inner_page animate-texts');
      parallax.destroy();
    }
  });

// Don't forget to init the view!

  Home.init()
  Logistics.init()
  Spirit.init()
  Inner.init()

});
// == END OF BRUT PRESENTATION JS ==